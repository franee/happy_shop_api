# Objectives
Design and develop API endpoints for consumption by native mobile and web applications.
# Requirements
* Design and implement REST-ful API endpoints. More details below.
* Use the JSON API specification
* Error handling
* Write tests with RSpec / Minitest
* Write an API documentation for these endpoints
* Use Ruby on Rails ~> 4.2 or greater
* Use a Postgres database
* Write code that is as similar to production-level code as possible, given the constraints on your time
* You have 1 week (7 days) for this take-home test
* If you have any questions, it is ok to contact us

# Backstory
Happy Shop is an e-commerce retailer specializing in beauty products. They have an existing
web platform and are planning to release native mobile applications. For that to happen, you are
tasked to build endpoints for the mobile applications to which will consume these endpoints.

### Models
* Product
  * Attributes:
    * name, string.
    * sold_out, boolean
    * category, string. Eg. makeup, tools, brushes
    * under_sale, boolean.
    * price, integer. Eg. 1000 ($10), 2500 ($25)
    * sale_price, integer. Eg. 1000 ($10), 2500 ($25)

# API endpoints
For the scope of this task, we will use a brand new Rails project with 1 or more models.

### Products
* Returns multiple products with attributes for each product.
* It should be able to accept mandatory and/or optional parameters to:
  * Filter products based on specific categories or price
  * Sorting products based on price
  * Paginate products
  * Combinations of above

### Product
* Returns data for a single product with it’s attributes.