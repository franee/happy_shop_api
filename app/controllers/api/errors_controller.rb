module Api
  class ErrorsController < ApplicationController
    def routing
      respond_error(ActionController::RoutingError.new("No route matches #{request.method} #{request.path}"))
    end
  end
end
