module Api
  module V1
    class ProductsController < ApplicationController
      def index
        @pi = ProductsIndex.new(index_params)

        render json: @pi.products, links: @pi.links
      end

      def show
        @product = Product.find(params[:id])

        render json: @product
      end

      private

      def index_params
        params.permit(:sort, filter: %i[category price join], page: %i[number size])
      end
    end
  end
end
