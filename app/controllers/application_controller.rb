class ApplicationController < ActionController::API
  rescue_from StandardError, with: :respond_error

  protected

  def respond_error(error)
    render json: build_error_json(error), status: get_error_http_status(error)
  end

  def build_error_json(error)
    {
      'errors' => [
        {
          status: get_error_http_status(error),
          source: { pointer: format('%s %s', request.method, request.path) },
          detail: get_error_message(error)
        }
      ]
    }.to_json
  end

  def get_error_http_status(error)
    if error.is_a?(ActiveRecord::RecordNotFound) || error.is_a?(ProductsIndex::InvalidPageError)
      Rack::Utils::SYMBOL_TO_STATUS_CODE[:not_found]
    elsif error.is_a?(ActionController::RoutingError)
      Rack::Utils::SYMBOL_TO_STATUS_CODE[:bad_request]
    else
      Rack::Utils::SYMBOL_TO_STATUS_CODE[:internal_server_error]
    end
  end

  def get_error_message(error)
    if Rails.env.production?
      error.message
    elsif error.backtrace
      [error.message, error.backtrace.join("\n")].join("\n")
    else
      error.message
    end
  end
end
