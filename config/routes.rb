Rails.application.routes.draw do
  # namespaced routes for api versioning
  namespace :api, defaults: { format: 'json' } do
    # standard apis that just points to the latest version
    resources :products, controller: 'v1/products', only: %i[index show]
    match 'product/:id', to: 'v1/products#show', via: :get

    # v1 api
    namespace :v1 do
      resources :products, only: %i[index show]
      match 'product/:id', to: 'products#show', via: :get
    end

    # add v2 here later
  end

  # catch everything else and give a routing error message
  match '*a', to: 'api/errors#routing', via: :all

  get '/', to: redirect('api/products')
end
