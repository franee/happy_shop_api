# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

abort('Only run this on development & production envs!') if Rails.env.test?

require 'faker'

1.upto(100) do
  price = Faker::Number.between(10, 100)

  Product.create(
    name:       Faker::Beer.name,
    sold_out:   Faker::Boolean.boolean(0.8),
    category:   Faker::Beer.style,
    under_sale: Faker::Boolean.boolean(0.3),
    price:      price,
    sale_price: (price * 0.80).to_i
  )
end
