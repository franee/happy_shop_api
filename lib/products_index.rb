class ProductsIndex
  include Rails.application.routes.url_helpers

  InvalidPageError = Class.new(StandardError)

  FILTER_FIELDS = %w[category price join].freeze

  SORT_MAP = {
    'price'  => 'price ASC',
    '-price' => 'price DESC'
  }.freeze

  COMPARISON_MAP = {
    '$lte' => '<=',
    '$lt'  => '<',
    '$gte' => '>=',
    '$gt'  => '>',
    '$eq'  => '=',
    '$ne'  => '!='
  }.freeze

  JOIN_OPERATORS = %w[AND OR].freeze

  MAX_PER_PAGE = 25

  def initialize(params)
    @params = params.to_unsafe_h

    check_page_problems!
  end

  def products
    @products ||= begin
                    Product
                      .where(where_condition)
                      .paginate(page: current_page, per_page: MAX_PER_PAGE)
                      .order(order_condition)
                  end
  rescue WillPaginate::InvalidPage
    raise InvalidPageError, "Invalid page: #{raw_page_num}"
  end

  def links
    {
      self: api_products_url(rebuild_params),
      first: api_products_url(rebuild_params.merge(first_page)),
      prev: api_products_url(rebuild_params.merge(prev_page)),
      next: api_products_url(rebuild_params.merge(next_page)),
      last: api_products_url(rebuild_params.merge(last_page))
    }
  end

  private

  def check_page_problems!
    raise(InvalidPageError, "Invalid page: #{raw_page_num}") if products.out_of_bounds?
  end

  # filters

  def condition_filters
    @filters ||= begin
                   if @params['filter']
                     @params['filter'].select { |k, _| FILTER_FIELDS.include?(k) }
                   else
                     {}
                   end
                 end
  end

  def where_condition
    join = condition_filters.delete('join')

    # make sure to check to avoid sql injection
    # default to AND
    join = 'AND' unless JOIN_OPERATORS.include?(join)

    h = {}

    condition_filters.each do |col, val|
      m = val.match(/^(\$([lg]te?|eq|ne))(.+)$/)

      if m
        operator = COMPARISON_MAP[m[1]]
        value = (m[3] =~ /\d+/ ? m[3].to_i : m[3])
        h[col] = Product.send(:sanitize_sql_for_assignment, ["#{col} #{operator} ?", value])
      else # default to equality
        h[col] = Product.send(:sanitize_sql_for_assignment, ["#{col} = ?", val])
      end
    end

    h.values.join(" #{join} ")
  end

  # sorting

  def order_condition
    SORT_MAP[@params['sort']] || 'id ASC'
  end

  # pagination, based on https://blog.codeship.com/the-json-api-spec/

  def total_pages
    @total_pages ||= products.total_pages
  end

  def raw_page_num
    @raw_page_num ||= @params.dig('page', 'number')
  end

  def current_page
    @current_page ||= begin
                        page = raw_page_num

                        if page =~ /\D/
                          raise InvalidPageError, "Invalid page: #{page}"
                        end

                        (page || 1).to_i
                      end
  end

  def first_page
    { page: { number: 1 } }
  end

  def last_page
    { page: { number: total_pages } }
  end

  # should be nil there no next page. see http://jsonapi.org/format/#fetching-pagination
  def next_page
    tmp_page = current_page + 1
    page = (tmp_page >= total_pages ? 'null' : tmp_page)

    { page: { number: page } }
  end

  # should be nil there is no previous page. see http://jsonapi.org/format/#fetching-pagination
  def prev_page
    tmp_page = current_page - 1
    page = (tmp_page <= 1 ? 'null' : tmp_page)

    { page: { number: page } }
  end

  def rebuild_params
    @rebuild_params ||= begin
                          rejected = %w[action controller format]
                          @params.reject { |key, _| rejected.include?(key.to_s) }
                        end
  end
end
