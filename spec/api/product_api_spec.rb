require 'rails_helper'

RSpec.describe '/api/product api', type: :request do
  include_examples 'product(s) api for show method', '/api', 'product'
  include_examples 'product(s) errors', '/api', 'product'
end
