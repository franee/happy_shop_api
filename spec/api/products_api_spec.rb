require 'rails_helper'

RSpec.describe '/api/products api', type: :request do
  include_examples 'common for /api/products & /api/v1/products', '/api'
  include_examples 'product(s) api for show method', '/api', 'products'
  include_examples 'product(s) errors', '/api', 'products'
  include_examples 'sorting for products api', '/api'
  include_examples 'filtering for products api', '/api'
  include_examples 'pagination for products api', '/api'
  include_examples 'combined parameters for products api', '/api/v1'
end
