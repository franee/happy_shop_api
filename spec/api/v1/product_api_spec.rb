require 'rails_helper'

RSpec.describe '/api/v1/product api', type: :request do
  include_examples 'product(s) api for show method', '/api/v1', 'product'
  include_examples 'product(s) errors', '/api/v1', 'product'
end
