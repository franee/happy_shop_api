require 'rails_helper'

RSpec.describe '/api/v1/products api', type: :request do
  include_examples 'common for /api/products & /api/v1/products', '/api/v1'
  include_examples 'product(s) api for show method', '/api/v1', 'products'
  include_examples 'product(s) errors', '/api/v1', 'products'
  include_examples 'sorting for products api', '/api/v1'
  include_examples 'filtering for products api', '/api/v1'
  include_examples 'pagination for products api', '/api/v1'
  include_examples 'combined parameters for products api', '/api/v1'
end
