FactoryGirl.define do
  factory :product do
    name       { Faker::Beer.name }
    sold_out   { Faker::Boolean.boolean(0.8) }
    category   { Faker::Beer.style }
    under_sale { Faker::Boolean.boolean(0.3) }
    price      { Faker::Number.between(10, 100) }
    sale_price { (price * 0.80).to_i }
  end
end
