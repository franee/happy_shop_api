require 'uri'

RSpec.configure do |config|
  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  config.shared_context_metadata_behavior = :apply_to_host_groups

  config.default_formatter = 'doc'

  config.order = :random

  Kernel.srand config.seed
end

# custom matchers

RSpec::Matchers.define :be_jsonapi_response_for do |model|
  match do |actual|
    parsed_actual = JSON.parse(actual)

    h = if parsed_actual['data'].is_a?(Array)
          parsed_actual['data'].first
        else
          parsed_actual['data']
        end

    h['type'] == model && h['attributes'].is_a?(Hash)
  end
end

RSpec::Matchers.define :be_jsonapi_error_response do |http_status|
  match do |actual|
    parsed_actual = JSON.parse(actual)

    errors = parsed_actual['errors']

    return false if errors.blank? || errors.first.blank?

    h = errors.first

    h['status'] == Rack::Utils::SYMBOL_TO_STATUS_CODE[http_status] &&
      h['source']['pointer'] == "#{request.method} #{request.path}"
  end
end

def get_comp_proc(operator)
  case operator
  when :lte
    proc { |a, b| a <= b }
  when :lt
    proc { |a, b| a < b }
  when :gte
    proc { |a, b| a >= b }
  when :gt
    proc { |a, b| a > b }
  when :ne
    proc { |a, b| a != b }
  when :eq
    proc { |a, b| a == b }
  end
end

def extract_hashes_by_type(data, field, field_value, operator)
  proc = get_comp_proc(operator)

  data.select do |h|
    proc.call(h['attributes'][field], field_value)
  end
end

RSpec::Matchers.define :be_jsonapi_filtered_response_for do |field, field_value, count, operator|
  match do |actual|
    parsed_actual = JSON.parse(actual)

    operator ||= :ne

    prod_hashes = extract_hashes_by_type(parsed_actual['data'], field, field_value, operator)

    prod_hashes.size == count
  end
end

RSpec::Matchers.define :be_jsonapi_multi_filtered_response_for do |filters, count, join|
  match do |actual|
    parsed_actual = JSON.parse(actual)

    h = {}

    filters.each do |field, arr|
      field_value, operator = arr

      h[field] = extract_hashes_by_type(parsed_actual['data'], field, field_value, operator)
    end

    products = if join == :and
                 h['category'] & h['price']
               elsif join == :or
                 h['category'] | h['price']
               else
                 []
               end

    products.size == count
  end
end

RSpec::Matchers.define :be_jsonapi_sorted_response_by do |field, sort_direction|
  match do |actual|
    parsed_actual = JSON.parse(actual)

    max = 0

    if sort_direction == :desc
      max = if field == 'id'
              parsed_actual['data'].first[field].to_i
            else
              parsed_actual['data'].first['attributes'][field]
            end
    end

    parsed_actual['data'].each do |h|
      value = if field == 'id'
                h[field].to_i
              else
                h['attributes'][field]
              end

      if sort_direction == :asc
        return false if max > value

        max = value
      elsif max < value
        return false
      else
        max = value
      end
    end

    true
  end
end

RSpec::Matchers.define :be_on_correct_page do |page|
  match do |actual|
    parsed_actual = JSON.parse(actual)

    str = "page[number]=#{page}"

    path = URI.unescape(parsed_actual['links']['self'])

    path.include?(str)
  end
end
