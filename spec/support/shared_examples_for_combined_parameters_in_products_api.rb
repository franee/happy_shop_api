RSpec.shared_examples 'combined parameters for products api' do |prefix|
  context 'combined parameters' do
    before(:each) do
      create(:product, category: 'Lager', price: 47)
      create(:product, category: 'Ale', price: 50)
      create(:product, category: 'Ale', price: 49)
      create(:product, category: 'Ale', price: 48)
      create(:product, category: 'Ale', price: 47)

      1.upto(2) do
        create(:product, category: 'Lager', price: 51)
      end
    end

    context 'filter_by :price and :category, sort_by :price' do
      context 'no pagination' do
        it 'should return correctly' do
          h = {
            filter: {
              price: '$lt50',
              category: 'Ale',
              join: 'AND'
            },
            sort: 'price'
          }

          get "#{prefix}/products", params: h

          filters = {
            'price'    => [50, :lt],
            'category' => ['Ale', :eq]
          }

          expect(response.body).to be_jsonapi_multi_filtered_response_for(filters, 3, :and)
          expect(response.body).to be_jsonapi_sorted_response_by('price', :asc)
        end
      end

      context 'with pagination ' do
        before(:each) do
          stub_const('ProductsIndex::MAX_PER_PAGE', 2)
        end

        it 'should return 2 pages' do
          h = {
            filter: {
              price: '$lt50',
              category: 'Ale',
              join: 'AND'
            },
            sort: 'price',
            page: { number: 1 }
          }

          get "#{prefix}/products", params: h

          filters = {
            'price'    => [50, :lt],
            'category' => ['Ale', :eq]
          }

          expect(response.body).to be_jsonapi_multi_filtered_response_for(filters, 2, :and)
          expect(response.body).to be_jsonapi_sorted_response_by('price', :asc)
        end
      end
    end

    context 'filter_by :price or :category, sort_by :price' do
      context 'no pagination' do
        it 'should return correctly' do
          h = {
            filter: {
              price: '$lt50',
              category: 'Ale',
              join: 'OR'
            },
            sort: 'price'
          }

          get "#{prefix}/products", params: h

          filters = {
            'price'    => [50, :lt],
            'category' => ['Ale', :eq]
          }

          expect(response.body).to be_jsonapi_multi_filtered_response_for(filters, 5, :or)
          expect(response.body).to be_jsonapi_sorted_response_by('price', :asc)
        end
      end

      context 'with pagination ' do
        before(:each) do
          stub_const('ProductsIndex::MAX_PER_PAGE', 3)
        end

        it 'should return 2 pages' do
          h = {
            filter: {
              price: '$lt50',
              category: 'Ale',
              join: 'OR'
            },
            sort: 'price',
            page: { number: 2 }
          }

          get "#{prefix}/products", params: h

          filters = {
            'price'    => [50, :lt],
            'category' => ['Ale', :eq]
          }

          expect(response.body).to be_jsonapi_multi_filtered_response_for(filters, 2, :or)
          expect(response.body).to be_jsonapi_sorted_response_by('price', :asc)
        end
      end
    end
  end
end
