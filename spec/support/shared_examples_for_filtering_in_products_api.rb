require 'uri'

RSpec.shared_examples 'filtering for products api' do |prefix|
  context 'filtering' do
    before(:each) do
      1.upto(2) do
        create(:product, category: 'Ale', price: 49)
      end

      1.upto(2) do
        create(:product, category: 'Bock', price: 50)
      end

      1.upto(2) do
        create(:product, category: 'Lager', price: 51)
      end
    end

    context 'filter_by :category' do
      let(:category) do
        'Ale'
      end

      context "category = 'Ale'" do
        context 'no $eq specified' do
          it 'should return 2 products' do
            get URI.escape("#{prefix}/products?filter[category]=#{category}")

            expect(response.body).to be_jsonapi_filtered_response_for('category', category, 2, :eq)
          end
        end

        context '$eq specified' do
          it 'should return 2 products' do
            get URI.escape("#{prefix}/products?filter[category]=$eq#{category}")

            expect(response.body).to be_jsonapi_filtered_response_for('category', category, 2, :eq)
          end
        end
      end

      context "category != 'Ale'" do
        context '$ne specified' do
          it 'should return 4 products' do
            get URI.escape("#{prefix}/products?filter[category]=$ne#{category}")

            expect(response.body).to be_jsonapi_filtered_response_for('category', category, 4, :ne)
          end
        end
      end
    end

    context 'filter_by :price' do
      context 'price = 50' do
        context 'no $eq specified' do
          it 'should return 2 products' do
            get URI.escape("#{prefix}/products?filter[price]=50")

            expect(response.body).to be_jsonapi_filtered_response_for('price', 50, 2, :eq)
          end
        end

        context '$eq specified' do
          it 'should return 2 products' do
            get URI.escape("#{prefix}/products?filter[price]=$eq50")

            expect(response.body).to be_jsonapi_filtered_response_for('price', 50, 2, :eq)
          end
        end
      end

      context 'price != 50' do
        context '$ne' do
          it 'should return 4 products' do
            get URI.escape("#{prefix}/products?filter[price]=$ne50")

            expect(response.body).to be_jsonapi_filtered_response_for('price', 50, 4, :ne)
          end
        end
      end

      context 'price <= 50' do
        context '$lte' do
          it 'should return 4 products' do
            get URI.escape("#{prefix}/products?filter[price]=$lte50")

            expect(response.body).to be_jsonapi_filtered_response_for('price', 50, 4, :lte)
          end
        end
      end

      context 'price < 50' do
        context '$lt' do
          it 'should return 2 products' do
            get URI.escape("#{prefix}/products?filter[price]=$lt50")

            expect(response.body).to be_jsonapi_filtered_response_for('price', 50, 2, :lt)
          end
        end
      end

      context 'price >= 50' do
        context '$gte' do
          it 'should return 4 products' do
            get URI.escape("#{prefix}/products?filter[price]=$gte50")

            expect(response.body).to be_jsonapi_filtered_response_for('price', 50, 4, :gte)
          end
        end
      end

      context 'price > 50' do
        context '$gt' do
          it 'should return 2 products' do
            get URI.escape("#{prefix}/products?filter[price]=$gt50")

            expect(response.body).to be_jsonapi_filtered_response_for('price', 50, 2, :gt)
          end
        end
      end
    end

    context 'filter_by :price or :category' do
      context "price > 50 or category = 'Ale'" do
        it 'should return 4 products' do
          get URI.escape("#{prefix}/products?filter[price]=$gt5&filter[category]=Ale&filter[join]=OR")

          filters = {
            'price'    => [50, :gt],
            'category' => ['Ale', :eq]
          }

          expect(response.body).to be_jsonapi_multi_filtered_response_for(filters, 4, :or)
        end
      end
    end

    context 'filter_by :price and :category' do
      context "price >= 50 or category = 'Bock'" do
        it 'should return 2 products' do
          get URI.escape("#{prefix}/products?filter[price]=$gte50&filter[category]=Bock&filter[join]=AND")

          filters = {
            'price'    => [50, :gte],
            'category' => ['Bock', :eq]
          }

          expect(response.body).to be_jsonapi_multi_filtered_response_for(filters, 2, :or)
        end
      end
    end
  end
end
