RSpec.shared_examples 'pagination for products api' do |prefix|
  context 'pagination' do
    before(:each) do
      stub_const('ProductsIndex::MAX_PER_PAGE', 1)

      1.upto(5) do |i|
        create(:product, price: i)
      end
    end

    context 'no page specified' do
      it 'should be in first page' do
        get "#{prefix}/products"

        expect(response.body).to be_on_correct_page(1)
      end
    end

    context 'page is specified' do
      context 'valid pages' do
        it 'should return the correct page' do
          1.upto(5).each do |page|
            get "#{prefix}/products?page[number]=#{page}"

            expect(response.body).to be_on_correct_page(page)
          end
        end
      end

      context 'invalid  page' do
        it 'should respond with an error' do
          [nil, '1a', 'a', '-'].each do |page|
            get "#{prefix}/products?page[number]=#{page}"

            expect(response.body).to be_jsonapi_error_response(:not_found)
          end
        end
      end

      context 'out-of-bounds page' do
        it 'should respond with an error' do
          [-1, 0, 6].each do |page|
            get "#{prefix}/products?page[number]=#{page}"

            expect(response.body).to be_jsonapi_error_response(:not_found)
          end
        end
      end
    end
  end
end
