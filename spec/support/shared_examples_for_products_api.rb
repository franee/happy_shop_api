RSpec.shared_examples 'should conform to the JSON API specification' do
  it 'should respond with proper json.api content_type' do
    do_request

    expect(response.content_type).to eq('application/vnd.api+json')
  end
end

RSpec.shared_examples 'should respond with an error json' do |http_status|
  it 'should respond with an error json' do
    do_request

    expect(response.body).to be_jsonapi_error_response(http_status)
  end
end

RSpec.shared_examples 'respond with links array' do
  it 'should always return with a links array' do
    do_request

    parsed_data = JSON.parse(response.body)
    expect(parsed_data['links']).to_not be_empty
  end
end

RSpec.shared_examples 'product(s) api for show method' do |prefix, resource|
  describe "GET #{prefix}/#{resource}/:id" do
    let!(:p) do
      create(:product)
    end

    let(:do_request) do
      get "#{prefix}/#{resource}/#{p.id}"
    end

    context 'existing product' do
      it 'should respond with product json' do
        do_request

        expect(response.body).to be_jsonapi_response_for('products')
      end

      include_examples 'should conform to the JSON API specification'
    end

    context 'not found' do
      let(:do_request) do
        get "#{prefix}/#{resource}/0"
      end

      it 'should respond with a not found status' do
        do_request

        expect(response).to have_http_status(:not_found)
      end

      include_examples 'should respond with an error json', :not_found
    end
  end
end

RSpec.shared_examples 'product(s) errors' do |prefix, resource|
  context 'server error' do
    let!(:p) do
      create(:product)
    end

    let(:do_request) do
      get "#{prefix}/#{resource}/#{p.id}"
    end

    before(:each) do
      allow(Product).to receive(:find) { raise 'server error' }
    end

    include_examples 'should respond with an error json', :internal_server_error
    include_examples 'should conform to the JSON API specification'
  end

  context 'routing errors' do
    context 'non-existent routes' do
      describe "GET #{prefix}/#{resource}does_not_exist" do
        let(:do_request) do
          get "#{prefix}/#{resource}does_not_exist"
        end

        include_examples 'should respond with an error json', :bad_request
        include_examples 'should conform to the JSON API specification'
      end

      describe "POST #{prefix}/#{resource}does_not_exist" do
        let(:do_request) do
          post "#{prefix}/#{resource}does_not_exist"
        end

        include_examples 'should respond with an error json', :bad_request
        include_examples 'should conform to the JSON API specification'
      end
    end

    context 'wrong request method, existing path' do
      describe "POST #{prefix}/#{resource}" do
        let(:do_request) do
          post "#{prefix}/#{resource}", params: { product: attributes_for(:product) }
        end

        include_examples 'should respond with an error json', :bad_request
        include_examples 'should conform to the JSON API specification'
      end
    end
  end
end

RSpec.shared_examples 'common for /api/products & /api/v1/products' do |prefix|
  describe "GET #{prefix}/products" do
    let(:do_request) do
      get "#{prefix}/products"
    end

    it 'responds successfully with an HTTP 200 status code' do
      do_request

      expect(response).to be_success
      expect(response).to have_http_status(:ok)
    end

    include_examples 'should conform to the JSON API specification'
    include_examples 'respond with links array'

    context 'no data' do
      it 'should return an empty data array' do
        do_request

        parsed_data = JSON.parse(response.body)

        expect(parsed_data['data']).to be_empty
      end

      include_examples 'should conform to the JSON API specification'
      include_examples 'respond with links array'
    end

    context 'with data' do
      before(:each) do
        1.upto(10) do
          create(:product)
        end
      end

      it 'should return a list of products' do
        do_request

        expect(response.body).to be_jsonapi_response_for('products')
      end

      include_examples 'should conform to the JSON API specification'
      include_examples 'respond with links array'
    end
  end
end
