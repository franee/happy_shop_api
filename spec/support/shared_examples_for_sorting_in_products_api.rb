require 'uri'

RSpec.shared_examples 'sorting for products api' do |prefix|
  context 'sorting' do
    before(:each) do
      1.upto(10) do |i|
        create(:product, price: (i + 10))
      end
    end

    context 'no sorting specified' do
      it 'should order by id ASC' do
        get "#{prefix}/products"

        expect(response.body).to be_jsonapi_sorted_response_by('id', :asc)
      end
    end

    context 'sort by price' do
      context 'price ASC' do
        it 'should order by price ASC' do
          get "#{prefix}/products?sort=price"

          expect(response.body).to be_jsonapi_sorted_response_by('price', :asc)
        end
      end

      context 'price DESC' do
        it 'should order by price DESC' do
          get "#{prefix}/products?sort=-price"

          expect(response.body).to be_jsonapi_sorted_response_by('price', :desc)
        end
      end
    end
  end
end
